# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8-80 compliant>
# Copyright 2015 Bassam Kurdali

if "bpy" in locals():
    import importlib
    importlib.reload(parambulator)
    importlib.reload(core)
    importlib.reload(duplicate_group)
    importlib.reload(warpcore)

else:
    from . import parambulator
    from . import duplicate_group
    from . import warpcore
    from . import core

import bpy
import math
from mathutils import Vector, Quaternion, Matrix, Euler, Color
from .parambulator import Foot, ParameterEffect, Parambulator
from .duplicate_group import make_new_group
from .warpcore import *


def get_strips_actions(ob):
    try:
        strips = ob.animation_data.nla_tracks[REFERENCE_TRACK].strips
    except:
        return [] # Don't make a fuss but we should poll elsewhere
    return [tuple([strip.action.name] * 3) for strip in strips]


def get_action_from_group(self, context):
    group = bpy.data.groups[context.scene.orox_crowd_group]
    for ob in group.objects:
        if ob.type == "ARMATURE" and ob.data.orox_ness:
            return get_strips_actions(ob)


def get_action_from_object(self, context):
    return get_strips_actions(self)


def get_action_from_active(self, context):
    return get_strips_actions(context.object)


class CrowdWalker(Parambulator):
    """ A Walking object that follows an empty from a crowd """

    subtype = 'CROWDWALKER'

# #################        Initialization, Factories          #################
    def __init__(self, walker_object, reference):
        super().__init__(walker_object)
        self.ref_object = reference
        self.reference_offset = Matrix()

    @classmethod
    def generate(cls, reference, group, action, use_link):
        """ Factory to make a new walker based on reference object """
        # find walker_object in group
        for ob in group.objects:
            if ob.type == "ARMATURE":
                if any(bone.walktype == 'body' for bone in ob.pose.bones):
                    # duplicate group objects into scene
                    new_group, walker_ob = make_new_group(group, use_link, ob)
                    walker_ob.orox_subtype = cls.subtype
                    walker_ob.orox_crowd_reference = reference.name
                    walker = CrowdWalker(walker_ob, reference)
                    walker.set_action(action)
                    walker_ob.matrix_world = Matrix()
                    return walker

    @classmethod
    def refresh(cls, context, objects):
        """ Factory to generate walker data based on presimmed walkers """
        scene = context.scene
        walkers = core.walkers()
        for ob in objects:
            if ob.type == 'ARMATURE' and ob.orox_subtype == cls.subtype:
                print(ob.name)
                walker = walkers[ob] # registry returns None if not a walker
                if not walker:
                    print("adding", ob.name, ob.orox_crowd_reference)
                    walker = CrowdWalker(
                        ob, scene.objects[ob.orox_crowd_reference])
                    print(walker.object)
                    action = ob.orox_reference_action
                    walker.set_action(action)
                    walkers.append(walker)

# ##########################  Footstep manangement  ###########################

    def make_footsteps(self, frames, reference_offset, follow):
        '''
        make foosteps offset from a reference object locations...
        '''

        # setup some variables:
        feet = self.feet
        bones = self.object.pose.bones
        # stride = self.actions[self.action] * self.footstep_stride_multiplier
        stride = self.stride * self.footstep_stride_multiplier
        offmat = self.reference_offset
        offsets = [self.get_offset(foot, offmat) for foot in feet]
        ref_object = self.ref_object
        for foot in feet:
            foot.steps = []

        # get animation data from our reference:
        location_curves = warpcore.object_location_curves(
            ref_object.animation_data.action)

        rotation_curves = warpcore.object_euler_curves(
            ref_object.animation_data.action)


        # Initial conditions:
        try:
            original_rotation = Euler(
                [curve.evaluate(frames[0]) for curve in rotation_curves],
                'XYZ').to_quaternion()
        except ValueError:
            raise WarpError(
                "Target {} lacks euler curves".format(ref_object.name))
        a = reference_offset.copy()
        a.rotate(original_rotation)
        try:
            original_location = a + Vector(
                [curve.evaluate(frames[0]) for curve in location_curves])
        except ValueError:
            raise WarpError(
                "Target {} lacks location curves".format(ref_object.name))
        original_frame = frames[0]
        # loop over frames, carving out footsteps...
        spins = []
        for frame in frames:
            current_rotation = Euler(
                [curve.evaluate(frame) for curve in rotation_curves],
                'XYZ').to_quaternion()
            a = reference_offset.copy()
            a.rotate(current_rotation)
            current_location = a + Vector(
                [curve.evaluate(frame) for curve in location_curves])


            distance = dist_get(original_location, current_location)
            if distance >= stride or frame == frames[-1]:
                vector = current_location - original_location
                stepmat = vector.to_track_quat('-Y','Z')
                if follow:
                    spin = - current_rotation.rotation_difference(
                        stepmat).to_euler('XYZ')[1]
                    spin = Euler((0, spin, 0), 'XYZ')
                    spins.append(spin)
                    stepmat.rotate(spin)
                stepmat = stepmat.to_matrix()

                for foot, offset in zip(feet, offsets):

                    foot.steps.append({
                        'location': stepmat * offset + original_location,
                        'frame': original_frame})
                original_location = current_location
                original_frame = frame
        # do some housekeeping, rotations, etc...
        self.add_footstep_rotations(0)
        for foot in self.feet:
            for index, spin in enumerate(spins):
                foot.steps[index]['rotation'].rotate(spin)


class WalkersAdd(bpy.types.Operator):
    """ Convert selected Objects into Walking Characters """

    bl_idname = 'object.crowd_walkers_add'
    bl_label = "Add Walkers to Reference Objects"

    group = bpy.props.EnumProperty(
        name="Crowd Group", items=duplicate_group.get_groups)
    use_link = bpy.props.BoolProperty(default=True, name="Link Objects")
    action = bpy.props.EnumProperty(
        name="Crowd Reference Action", items=get_action_from_group)
    offset = bpy.props.FloatVectorProperty()

    @classmethod
    def poll(cls, context):
        return context.selected_objects

    def execute(self, context):
        walkers = core.walkers()
        scene = context.scene
        group = bpy.data.groups[self.properties.group]
        try:
            action = bpy.data.actions[self.properties.action]
        except (TypeError, KeyError):
            self.report({'ERROR'}, "No Reference Action")
            return {'CANCELLED'}
        offset = Vector(self.properties.offset)
        context.window.cursor_modal_set('WAIT')
        for ob in context.selected_objects:
            if ob.animation_data and ob.animation_data.action:
                walkers.append(
                    CrowdWalker.generate(
                        ob, group, action, self.properties.use_link))
        for walker in walkers:
            try:
                walker.make_footsteps(
                    range(scene.frame_start, scene.frame_end), offset, True)
            except WarpError as e:
                 self.report({'ERROR'}, e.args[0]) # Let the user know
                 return {'CANCELLED'}
            walker.bake_walk()
        context.window.cursor_modal_restore()
        return {'FINISHED'}


class WalkerReSim(bpy.types.Operator):
    """ Resim Selected Walkers """

    bl_idname = 'object.crowd_walkers_resim'
    bl_label = "Resim Selected Crowd Walkers"

    action = bpy.props.EnumProperty(
        name="Crowd Reference Action", items=get_action_from_active)

    offset = bpy.props.FloatVectorProperty()

    @classmethod
    def poll(cls, context):
        return all(
            ob.type == 'ARMATURE' and ob.orox_subtype == CrowdWalker.subtype
            for ob in context.selected_objects)

    def execute(self, context):
        walkers = core.walkers()
        scene = context.scene
        try:
            action = bpy.data.actions[self.properties.action]
        except (TypeError, KeyError):
            self.report({'ERROR'}, "No Reference Actions")
            return {'CANCELLED'}
        offset = Vector(self.properties.offset)
        context.window.cursor_modal_set('WAIT')
        for ob in context.selected_objects:
            walker = walkers[ob]
            if not walker:
                print("Creating Walker")
                walker = CrowdWalker(
                    ob, scene.objects[ob.orox_crowd_reference])
                walker.set_action(action)
                walkers.append(walker)
            print(walker.object)
            if walker.action != action:
                walker.set_action(action)
            try:
                walker.make_footsteps(range(
                    scene.frame_start, scene.frame_end), offset, True)
            except WarpError as e:
                self.report({'ERROR'}, e.args[0])
            walker.bake_walk()
        context.window.cursor_modal_restore()
        return {'FINISHED'}


class CrowdPanel(bpy.types.Panel):
    """ Orox Crowd Panel """

    bl_label = "Orox Crowds"
    bl_idname = "SCENE_PT_orox_crowds"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = 'OROX'

    @classmethod
    def poll(cls, context):
        return len(bpy.data.actions)

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        ob = context.object

        row = layout.row()
        row.column().prop(scene, 'orox_crowd_offset', text='Offset')
        row.column().label("")

        box = layout.box()
        box.row().label("ReSim Selection")
        row = box.row()
        is_orox = (
            ob.type == "ARMATURE" and
            ob.data.orox_ness and
            ob.orox_subtype == CrowdWalker.subtype)
        if is_orox:
            row.prop(ob, 'orox_reference_action', text='Walk Action')
        else:
            row.label('No Active Walker')
        resim = row.operator(
            WalkerReSim.bl_idname, text="Resim selected")
        if is_orox:
            resim.action = ob.orox_reference_action
            resim.offset = scene.orox_crowd_offset

        box = layout.box()
        box.row().label("Generate from Objects")
        row = box.row()
        row.prop(scene, 'orox_crowd_group')
        row.prop(scene, 'orox_crowd_use_link')
        row = box.row()
        row.prop(scene, 'orox_crowd_reference_action', text='Reference Action')
        walkers_add = row.operator(WalkersAdd.bl_idname, text="Create")
        walkers_add.group = scene.orox_crowd_group
        walkers_add.use_link = scene.orox_crowd_use_link
        walkers_add.action = scene.orox_crowd_reference_action
        walkers_add.offset = scene.orox_crowd_offset


def register():

    core.walkers().register(CrowdWalker)
    bpy.types.Scene.orox_crowd_group = bpy.props.EnumProperty(
        name="Crowd Group", items=duplicate_group.get_groups)
    bpy.types.Scene.orox_crowd_use_link = bpy.props.BoolProperty(
        default=True, name="Link Objects")
    bpy.types.Scene.orox_crowd_reference_action = bpy.props.EnumProperty(
        name="Crowd Reference Action", items=get_action_from_group)
    bpy.types.Scene.orox_crowd_offset = bpy.props.FloatVectorProperty(
        subtype='TRANSLATION')

    bpy.types.Object.orox_reference_action = bpy.props.EnumProperty(
        name="Orox Reference Action", items=get_action_from_object)
    bpy.types.Object.orox_crowd_reference = bpy.props.StringProperty()

    bpy.utils.register_class(WalkersAdd)
    bpy.utils.register_class(WalkerReSim)
    bpy.utils.register_class(CrowdPanel)


def unregister():

    bpy.utils.unregister_class(CrowdPanel)
    bpy.utils.unregister_class(WalkersAdd)
    bpy.utils.unregister_class(WalkerReSim)
    del(bpy.types.Scene.orox_crowd_offset)
    del(bpy.types.Scene.orox_crowd_group)
    del(bpy.types.Scene.orox_crowd_use_link)
    del(bpy.types.Scene.orox_crowd_reference_action)

    del(bpy.types.Object.orox_crowd_reference)
    del(bpy.types.Object.orox_reference_action)
