# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8-80 compliant>
# Copyright 2015 Bassam Kurdali

bl_info = {
    "name": "Orox",
    "author": "Bassam Kurdali",
    "version": (3, 1),
    "blender": (2, 78, 0),
    "location": "",
    "description": "Walking Characters",
    "warning": "",
    "wiki_url": "",
    "category": "Animation"}

"""
Orox (Pronounced Or-Ox) is suite of tools for managing walking characters,
either crowds or individuals, based on user-created walkcycles.
"""

if "bpy" in locals():
    import importlib
    importlib.reload(tag)
    importlib.reload(core)
    importlib.reload(crowd_walkers)
    # TODO import nla_walkers from ED project

else:
    from . import tag
    from . import core
    from . import crowd_walkers

import bpy


def register():
    core.register()
    tag.register()
    crowd_walkers.register()

def unregister():
    tag.unregister()
    crowd_walkers.unregister()
    core.unregister()

if __name__ == '__main__':
    register()

