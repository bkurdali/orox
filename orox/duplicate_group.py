# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

import bpy
import re


def get_new_name(ob, oblist):
    ''' increment number at the end of the name '''
    pattern = re.compile("{}_\\d+".format(ob.name))  # matches on obname_nn
    similars = [obj for obj in oblist if pattern.match(obj.name)]
    similars.sort(key=lambda x: x.name)
    try:
        number = int(similars[-1].name.split('_')[-1]) + 1
    except IndexError:
        number = 1
    return "{0}_{1:04d}".format(ob.name, number)  # padding


def duplicate_object(ob, link_data=False):
    ''' duplicate a single object, appending _nn to name '''
    scene = bpy.context.scene
    new_name = get_new_name(ob, bpy.data.objects)
    if (link_data and 'DUPL_unique' not in ob.keys()) or not ob.data:
        new_data = ob.data
    else:
        new_data = ob.data.copy()
        new_data.name = new_name

    new_object = ob.copy()
    new_object.name = new_name
    new_object.data = new_data
    for slot in new_object.material_slots:
        old_material = slot.material
        slot.material = old_material.copy()
    scene.objects.link(new_object)
    new_object.layers = scene.layers
    if not ob.parent:
        new_object.location = scene.cursor_location
    return new_object


def duplicate_objects(object_list, link_data=False):
    ''' duplicates a bunch of objects from a list, appending _nn to name '''
    return [duplicate_object(ob, link_data) for ob in object_list]


def ignore_this(ob):
    ''' True if we don't want to duplicate this object '''
    return ob.name.startswith('SHA_')


def sorted_items(items):
    sorted_list = [itm for itm in items]
    sorted_list.sort(key=lambda x:x.name)
    return sorted_list


def zip_up(group_A, group_B):
    ''' zip it! '''
    objects_A = sorted_items(group_A.objects)
    objects_B = sorted_items(group_B.objects)
    return [itm for itm in zip(objects_A, objects_B)]


def equivelant(obj, groups):
    ''' find the equivelant to object in groups[0] '''
    if '_' in groups[1].name:
        old_suffix = groups[1].name.split('_')[-1]
        try:
            num = int(old_suffix)
        except:
            prefix = obj.name
        else:
            split = obj.name.split('_')
            prefix = '_'.join(split[:-1])
            if not prefix: prefix = split[0]
    else:
        prefix = obj.name
    suffix = groups[0].name.split('_')[-1]
    new_name = "{}_{}".format(prefix, suffix)
    print(obj.name, new_name)
    new_object = [ob for ob in groups[0].objects if ob.name==new_name][0]
    return new_object


def fixup_parenting(A, B, groups):
    ''' parent to the object in the right group '''
    old_parent = B.parent
    if not old_parent:
        return
    if old_parent.name in groups[0].objects:
        A.parent = old_parent
    elif old_parent.name in groups[1].objects:
        A.parent = equivelant(old_parent, groups)


def fixup_datablock_drivers(animation_data, groups):
    ''' fixup driver targets on a specific datablock '''
    for driver in animation_data.drivers:
        for variable in driver.driver.variables:
            for target in variable.targets:
                old_target = target.id
                nom = old_target.name
                if nom in groups[1].objects and nom not in groups[0].objects:
                    target.id = equivelant(old_target, groups)


def fixup_drivers(A, B, groups):
    ''' we've got object, data, and material drivers to deal with '''
    if A.animation_data:
        fixup_datablock_drivers(A.animation_data, groups)
    if A.data:
        if A.data.animation_data:
            fixup_datablock_drivers(A.animation_data, groups)
        if 'shape_keys' in dir(A.data):
            if A.data.shape_keys and A.data.shape_keys.animation_data:
                fixup_datablock_drivers(
                    A.data.shape_keys.animation_data,
                    groups)
    for slot in A.material_slots:
        if slot.material and slot.material.animation_data:
            fixup_datablock_drivers(slot.material.animation_data, groups)

def fixup_cons_or_mods(holder, groups):
    ''' works for either a constraint or a modifier '''
    for prop in dir(holder):
        old_thing = getattr(holder, prop)
        if type(old_thing) == bpy.types.Object:
            nom = old_thing.name
            if nom in groups[1].objects and nom not in groups[0].objects:
                new_thing = equivelant(old_thing, groups)
                setattr(holder, prop, new_thing)


def fixup_modifiers(A, B, groups):
    ''' now to fix up any and all modifier targets '''
    for modifier in A.modifiers:
        fixup_cons_or_mods(modifier, groups)


def fixup_constraints(A, B, groups):
    ''' fix up constraint targets '''
    for constraint in A.constraints:
        fixup_cons_or_mods(constraint, groups)


def fixup_relations(object_tuples, groups):
    ''' we need a way to copy over relations from oblists'''
    for object_tuple in object_tuples:
        fixup_parenting(object_tuple[0], object_tuple[1], groups )
        fixup_drivers(object_tuple[0], object_tuple[1], groups)
        fixup_constraints(object_tuple[0], object_tuple[1], groups)
        fixup_modifiers(object_tuple[0], object_tuple[1], groups)
        if object_tuple[0].type == 'ARMATURE':
            for bone0 in object_tuple[0].pose.bones:
                bone1 = object_tuple[1].pose.bones[bone0.name]
                fixup_constraints(bone0, bone1, groups)


def make_new_group(group, link_data=False, walker=None):
    ''' creates a high functioning copy of the given group '''
    old_objects = [ob for ob in group.objects if ignore_this(ob)]
    new_objects = duplicate_objects(
        [
            ob for ob in group.objects
            if not ob in old_objects and ob != walker], link_data)
    if walker:
        walker = duplicate_object(walker, link_data)
        new_objects.append(walker)
    new_group_name = get_new_name(group, bpy.data.groups)
    new_group = bpy.data.groups.new(name=new_group_name)
    for ob in new_objects + old_objects:
        new_group.objects.link(ob)
    for ob in new_objects:
        try:
            group.objects.unlink(ob)
        except:
            pass
    for setting in ():  # any group setting we want to copy
        setattr(new_group, setting, getattr(group, setting))
    fixup_relations(zip_up(new_group, group),(new_group, group))
    return new_group, walker


def get_groups(self, context):
    '''
    return a list of groups in the scene
    '''
    pattern = re.compile(r'[ \w]+_[0-9]+')
    return [
        tuple([itm.name] * 3)
        for itm in bpy.data.groups if not pattern.match(itm.name)]   
