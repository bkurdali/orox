# Parambulator is the base class for a walking character, contains all the
# smarts needed for walking based on footsteps. Inherited Classes define
# Footstep generation algorithms and factory classmethods
# Copyright (C) 2012  Bassam Kurdali
#
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8-80 compliant>

"""
 Stuff
"""

if "bpy" in locals():
    import importlib
    importlib.reload(warpcore)

else:
    from . import warpcore

import bpy

from pprint import pprint
from mathutils import Vector, Quaternion, Matrix, Euler, Color
from .warpcore import *

DEBUG = False

class Foot():
    """
    Simple foot structure:
        name (str) is the name of the foot bone
        tracks list_of_strings is the number of 'children' of the foot
        deps is a list of feet that the foot depends on
        up string in (X,Y,Z,-X,-Y,-Z)
        forward string in (X,Y,Z,-X,-Y,-Z)
    """

    def __init__(self, name, tracks, deps, up, forward):
        self.name = name
        self.tracks = tracks
        self.deps = deps
        self.up = up
        self.forward = forward
        self.steps = []


class ParameterEffect():
    """
    Structure that describes how a parameter effects a bone:
        bone (str) name of bone that is effected
        mutiplier (boolean) if true multiply the value, otherwise add
        transform (str) in transformation type (location, rotation_*, scale)
        'w': (boolean) do we affect this axis
        'w_mul': (float) multiplier for effect
        'x': (boolean)
        'x_mul': (float)
        'y': (boolean)
        'y_mul': (float)
        'z': (boolean)
        'z_mul': (float)
    """

    def __init__(
            self, name, bone, tform, multiplier,
            w, w_mul, x, x_mul, y, y_mul, z, z_mul):
        self.name = name
        self.tform = tform
        self.bone = bone
        self.multiplier = multiplier
        self.w = w
        self.w_mul = w_mul
        self.x = x
        self.x_mul = x_mul
        self.y = y
        self.y_mul = y_mul
        self.z = z
        self.z_mul = z_mul


class Parambulator(object):
    """ An individual walker that can walk based on footsteps """

# #################        Initialization Functions           #################

    def __init__(self, walker_object):

        self.object = walker_object
        self.data = walker_object.data
        self.bones = self.object.pose.bones
        self._set_params()
        self._add_param_ui()
        # Add the body to the head of our Feet list:
        try:
           body = self._find_uniques('body') # TODO multiple bodies
        except:
            pass # TODO error
        self.feet = [
            Foot(
                body, [],
                [dep.foot_number for dep in self.bones[body].foot_depends],
                self.data.up_vector, self.data.forward_vector),]
        # Now add the rest of the feet:
        self._fill_feet()
        self._fill_leg_tracks()
        self.root, self.properties = [
            self._find_uniques(unique)
            for unique in ('root', 'properties')
        ]
        if self.properties:
            for param in self.params:
                if param not in self.bones[self.properties].keys():
                    self.bones[self.properties][param] = 0.0
        self.in_air_start = 0.2 # defaults.in_air_start
        self.in_air_end = 0.8 # defaults.in_air_end
        self.flip = False
        self.walk_data = {}
        self.walkcycles_curves = {}
        self.footstep_stride_multiplier = 1.0
        self.stride = 1.0

    def _set_params(self):
        ''' finds and aggregrates all the paramters on all the bones'''
        self.params = {}
        for bone in self.bones:
            for param in bone.autowalkparams:
                # there's probably a better way to introspect this:
                param_list = (
                    a for a in dir(param) if not a in
                    dir(bpy.types.PropertyGroup) and
                    not a.startswith('__') and
                    not a == 'template_list_controls'
                    and not a == 'rna_type')
                param_dict = {
                    pname: getattr(param, pname) for pname in param_list}
                param_dict['bone'] = bone.name
                param_effect = ParameterEffect(**param_dict)
                if param.name in self.params.keys():
                    self.params[param.name].append(param_effect)
                else:
                    self.params[param.name] = [param_effect]

    def _add_param_ui(self):
        """ Adds Parameters to the UI """
        ob = self.object
        # Find pre existing paramters on our object:
        pre_existing = [old.name for old in ob.autowalker_params]

        # Add our new ones
        for param in self.params:
            if param not in pre_existing:
                param_ui = ob.autowalker_params.add()
                param_ui.name = param
            else:
                pre_existing.remove(param)

        # Purge any old ones left over
        for remaining in pre_existing:
            ob.autowalker_params.remove(ob.autowalker_params.find(remaining))

    def _fill_feet(self):
        """finds all the foot bones in the rig"""
        up = self.data.up_vector
        forward = self.data.forward_vector
        foot_props = [
            prop for prop in dir(bpy.types.PoseBone) if
            prop.startswith('leg_')] + ["name"]
        feet = [
            {prop: getattr(bone, prop) for prop in foot_props} for
            bone in self.bones if bone.walktype == "foot"]
        feet = sorted(feet, key=lambda foot: foot["leg_number"])
        self.feet.extend(
            Foot(foot['name'], [], [], up, forward) for foot in feet)

    def _fill_leg_tracks(self):
        leg_tracks = [bone for bone in self.bones if bone.walktype == 'leg']
        foot_props = [
            prop for prop in dir(bpy.types.PoseBone) if
            prop.startswith('leg_')]
        for foot_no in self.feet:
            foot = self.bones[foot_no.name]
            foot_no.tracks.extend([
                bone.name for bone in leg_tracks if
                all([getattr(bone, attr) == getattr(foot, attr) for
                    attr in foot_props])])

    def _find_uniques(self, unique):
        """ Get unique bones like properties, root, and stride """
        for bone in self.bones:
            if bone.walktype == unique:
                return bone.name

    def set_action(self, action):
        """ add a reference action to a parambulator """
        if type(action) == str:
            self.action = action
        else:
            self.action = action.name
        self._set_stride()
        self.injest_action()
        # self.prep_action(blank=True)

# ##################    Walking Functions and Helpers       ###################

    def get_offset(self, foot, offmat):
        """
        calculate an initial offset per bone
        """
        obj = self.object
        action = self.action
        walk_data = self.walk_data[action]

        transform = walk_data['offsets'][foot.name]
        bone_world_mat = Matrix.Translation(transform)
        offset_inv = bone_world_mat.inverted() * obj.matrix_world * offmat
        return offset_inv.inverted().to_translation()

    def offset_targets(self):
        """
        create the initial conditions we need for a smooth walk
        feet are data structures
        targets are pose bones
        """
        feet = self.feet
        bones = self.object.pose.bones
        stride = self.actions[self.action]
        walk_data = self.walk_data[self.action]
        for foot in feet:
            target = bones[foot.target]
            mat = walk_data['mats'][foot.name]
            offset = world_location_to_bone(
                walk_data['offsets'][foot.name],
                target.id_data, target.name)
            scale = mat.to_scale().length
            new_offset = (scale / stride) * offset
            # target.location = new_offset  let's try being dumb instead:
            # FIXME needs to take stride multiplier into account
            target.location = offset

    def _set_stride(self):
        """ get the stride from the reference action """
        self.stride = get_stride(
            self.feet[0].name,
            bpy.data.actions[self.action], self.object)

    def calculate_body(self, body, steps):
        """
        get the body location and frame based on it's foot-steps
        """
        data = self.walk_data[self.action]
        frames = [step['frame'] for step in steps]
        frame = int(average(frames))
        locations = [step['location'] for step in steps]
        originals = []
        for i in range(len(steps)):
            footbone = self.feet[body.deps[i]].name
            originals.append(data['offsets'][footbone])
        # step 1 find the average location.
        average_step = average(locations)
        # step 2 for each location find the walkcycle warped body location
        body_original = data['offsets'][body.name]
        warped_locations = []
        for index, location in enumerate(locations):
            original = data['offsets'][self.feet[body.deps[index]].name]
            warped_locations.append(location + body_original - original)
        # step 3 average the results of step 2
        body_average = average(warped_locations)
        # step 4 find the average body-foot distance in the walkcycle
        average_distance_cycle = average_distance(body_original, originals)
        # step 5 find the average body-foot distance currently
        average_distance_step = average_distance(body_average, locations)
        # step 6 move the position on the line between step 3
        # and step 1 to minimize step 5

        #average_distance_cycle *= 1.3

        result = body_location_optimize(
            body_average, average_distance_cycle,
            average_step, locations, [])

        #result = body_average
        new_name = "_B{}_{}_{}".format(
            self.object.name, self.feet.index(body), frame)
        props = {'frame': frame, 'name': body.name}
        return({'frame': frame, 'location': result})

    def blank_action_copy(self, nuevo, curveset=None):
        """
        copy curves from an action into a nuevo action, optionally restrict by
        curveset, exact implementation of restriction TBD
        """
        action = bpy.data.actions[self.action]
        #copy channels from action
        for group in action.groups:
            if not group.name in nuevo.groups:
                nuevo.groups.new(name=group.name)
        for fcurve in action.fcurves:
            try:
                    nuevo.fcurves.new(
                        data_path=fcurve.data_path,
                        index=fcurve.array_index,
                        action_group=\
                            "" if not fcurve.group else fcurve.group.name)
            except RuntimeError:
                old_curve = [
                    fc for fc in nuevo.fcurves if
                    fc.data_path == fcurve.data_path and
                    fc.array_index == fcurve.array_index][0]
                nuevo.fcurves.remove(old_curve)
                nuevo.fcurves.new(
                    data_path=fcurve.data_path,
                    index=fcurve.array_index,
                    action_group=\
                        "" if not fcurve.group else fcurve.group.name)
        return nuevo

    def injest_action(self):
        """
        get the walk matrixes time limits out of the reference action
        """
        if self.action in self.walk_data:
            return
        flip = self.flip
        source_walkcycle = bpy.data.actions[self.action]
        walkcycle_curves = group_curves(source_walkcycle.fcurves)
        old_frames = [int(frame) for frame in source_walkcycle.frame_range]

        # now we need the old offsets for any bone in feet or main target track
        walkcycle_mats = {}
        offsets = {}
        contacts = {}
        for foot in self.feet:
            # we assume contacts are at the first and last frame
            # start and end location are important to us
            location_curves = walkcycle_curves[foot.name]['location']
            start_location = Vector([
                curve.evaluate(old_frames[0]) for curve in location_curves])
            end_location = Vector([
                curve.evaluate(old_frames[1]) for curve in location_curves])
            distance = (end_location - start_location).length
            # find out where the foot is off the ground
            # evaluate the curve and set frame percentages:
            start = old_frames[0]
            end = old_frames[1]
            for frame in range(old_frames[0], old_frames[1]):
                location = Vector([
                    curve.evaluate(frame) for curve in location_curves])
                percentage = (location - start_location).length / distance
                if percentage < self.in_air_start:
                    start = frame
                elif percentage < self.in_air_end:
                    end = frame
                else:
                    break
            # FIXME The above essentially assumes monotonically increasing
            # distance which is not neccessarily true: something bad might
            # happen for off-beat walkcycles
            # now put everything in world coordinates for sanity
            start_location = bone_to_world(
                start_location, self.object, foot.name, True)
            end_location = bone_to_world(
                end_location, self.object, foot.name, True)
            # stuff our actual data in
            walkcycle_mats[foot.name] = offset_matrix(
                start_location, end_location,
                flip_axis(foot.forward, flip), foot.up)
            offsets[foot.name] = start_location
            contacts[foot.name] = (start, end)
        self.walk_data[self.action] = {'limits': old_frames,
            'mats': walkcycle_mats,
            'offsets': offsets,
            'contacts': contacts}

    def prep_action(self, blank=True):
        """
        prep our blank action for a good go
        """
        source_walkcycle = bpy.data.actions[self.action]
        if not self.action in self.walkcycles_curves.keys():
            walkcycle_curves = group_curves(source_walkcycle.fcurves)
            self.walkcycles_curves[self.action] = walkcycle_curves
        else:
            walkcycle_curves = self.walkcycles_curves[self.action]
        obj = self.object
        if not obj.animation_data:
            obj.animation_data_create()
        if obj.animation_data.action:
            # XXX Insure walker actions are unique!!!!
            target_walk = obj.animation_data.action
            if blank:
                self.blank_action_copy(target_walk)
        else:
            target_walk = bpy.data.actions.new(name="forward")
            obj.animation_data.action = target_walk
            self.blank_action_copy(target_walk)
        self.walk_curves = group_curves(target_walk.fcurves)
        body_tracks = set([
            bone for bone in self.walk_curves if bone not in [
                foot.name for foot in self.feet]])
        for foot in self.feet[-1:]:
            body_tracks = body_tracks.difference(set(foot.tracks))
        for bone in list(body_tracks):
            self.feet[0].tracks.append(bone) # XXX only one body!!!!

    def copy_transforms(
            self,
            pose_bone, loc_list,
            time_map, step_rots, step_mat, time_ramp, foot):
        """
        make walk keyframes in the new curves based on the old curves.
        """
        #assume that keyframes line up
        walk_data = self.walk_data[self.action]
        old_curves = self.walkcycles_curves[self.action]
        new_curves = self.walk_curves
        walk_mat = walk_data['mats'][foot.name]
        contacts = walk_data['contacts'][foot.name]
        limits = walk_data['limits']

        bone = pose_bone.name
        rotation_mode = pose_bone.rotation_mode
        obj = self.object
        data = self.data
        data_bone = data.bones[bone]

        rest_mat = obj.matrix_world * data_bone.matrix_local

        for transform in old_curves[bone]:

            length = 4 if transform in [
                'rotation_quaternion', 'rotation_axis_angle'] else 3
            offmult = [
                Vector((0 for i in range(length))),
                Vector((1 for i in range(length)))]
            axes = ['w', 'x', 'y', 'z'][4 - length:]
            for param in pose_bone.autowalkparams:
                if param.tform == transform:
                    scaler = obj.autowalker_params[param.name].value
                    if param.multiplier:
                        value = [
                            getattr(param, "{}_mul".format(axis)) **
                            (scaler * getattr(param, axis))
                            for axis in axes]
                        offmult[1] = Vector([
                            offmult[1][n]*value[n] for n in range(length)])
                    else:
                        value = [
                            getattr(param, axis) *
                            getattr(param, "{}_mul".format(axis)) * scaler
                            for axis in axes]
                        offmult[0] = offmult[0] + Vector(value)

            old_keyframes = keyframes_from_curves(old_curves[bone][transform])
            new_transform = new_curves[bone][transform]
            use_deform = pose_bone.walkdeform and any([
                trigger in transform for trigger in ('location', 'rotation')])

            for keyframe_set in old_keyframes:
                if use_deform and 'location' in transform:
                    local_location = pose_bone.id_data.data.bones[
                        bone].use_local_location
                    overrides = warp_core_location(
                        keyframe_set, time_ramp, offmult, limits,
                        step_mat, walk_mat, rest_mat,
                        local_location)
                elif use_deform and 'rotation' in transform:
                    flip = False # TODO maybe pull from user selection
                    overrides = warp_core_rotation(
                        keyframe_set, time_ramp, offmult, limits, flip, foot,
                        rotation_mode, step_rots, step_mat, walk_mat, rest_mat,
                        contacts)
                else:
                    overrides = warp_core_nodeform(
                        keyframe_set, time_ramp, offmult, limits)
                for curve in new_transform:
                    curve.keyframe_points.add()
                new_keyframes = [
                    curve.keyframe_points[-1] for curve in new_transform]
                for old_keyframe, new_keyframe, override in zip(
                        keyframe_set, new_keyframes, overrides):
                    copy_coords(old_keyframe, new_keyframe, override)

    def bake_steps(
        self, foot, anim_bones):
        """
        bake the steps from a foot that has steps onto anim_bones
        """
        walk_data = self.walk_data[self.action]
        flip = self.flip
        old_start = walk_data['limits'][0]
        old_end = walk_data['limits'][1]
        cycle_len = old_end - old_start
        penultimate_step = len(foot.steps) - 2
        for index, footstep in enumerate(foot.steps[:-1]):
            nextstep = foot.steps[index + 1]
            loc_list = [footstep['location'], nextstep['location']]
            step_mat = offset_matrix(
                loc_list[0], loc_list[1],
                flip_axis(foot.forward, flip), foot.up)
            step_rots = (
                footstep['rotation'], foot.steps[index + 1]['rotation'])
            if index < penultimate_step:
                loc_list.append(foot.steps[index + 2]['location'])
            time_map = (footstep['frame'], nextstep['frame'])
            step_time = time_map[1] - time_map[0]
            multiplier = step_time / cycle_len
            offset = time_map[0] - multiplier * old_start
            for bone in anim_bones:
                self.copy_transforms(
                    bone, loc_list, time_map,
                    step_rots, step_mat, (offset, multiplier), foot)

    def bake_walk(self):
        """
        This populates each walker action with it's new modified curves.
        """
        self.prep_action(blank=True) # Prep a Blank to store the sim
        pbones = self.object.pose.bones
        for foot in self.feet:  # copy each foot's steps individually
            footbones = [bone for bone in foot.tracks] + [foot.name]
            bones = [
                bone for bone in self.walkcycles_curves[self.action]
                if bone in footbones]
            anim_bones = [bone for bone in pbones if bone.name in bones]
            self.bake_steps(foot, anim_bones)

# ##########################  Footstep manangement  ###########################

    def add_footstep_rotations(self, spin):
        '''
        add rotations into footstep series
        '''
        for foot in self.feet:
            for index, step in enumerate(foot.steps):
                current = step['location']
                vector = foot.steps[
                    min(len(foot.steps) - 1, index + 1)]['location'] -\
                    foot.steps[max(0, index - 1)]['location']

                step['rotation'] = vector.to_track_quat('Y','Z')
                step['rotation'].rotate(Euler((0, spin, 0) , 'XYZ'))

    def injest_footsteps(self, scene):
        '''
        swallow the drawn steps as footsteps, presumably after user editing
        '''
        data = self.walk_data[self.action]
        feet = self.feet
        prefix = "_S{}_".format(self.object.name)
        obs = [ob for ob in scene.objects if ob.name.startswith(prefix)]
        for foot in feet:
            foot.steps = []
        steps = {}

        for ob in obs:
            frame = ob['frame']
            foot = ob['foot']
            location = ob.location
            rotation = ob.rotation_quaternion
            try:
                steps[foot].append({
                    'frame': frame, 'location': location, 'rotation': rotation})
            except:
                steps[foot] = [{
                    'frame': frame, 'location': location, 'rotation': rotation}]

        indie_feet = [foot for foot in self.feet if foot.deps == []]
        bodies = [foot for foot in self.feet if foot not in indie_feet]
        for foot in indie_feet:
            foot.steps = sorted(steps[foot.name], key=lambda x: x['frame'])
            step_no = len(foot.steps)
        for body in bodies:
            bodyfeet = [feet[index] for index in body.deps]

            step_groups = [
                [foot.steps[i] for foot in bodyfeet] for i in range(step_no)]
            bodysteps = [
                self.calculate_body(body, s) for s in step_groups]
            body.steps = bodysteps
            self.add_footstep_rotations(0)

    def remove_footsteps(self, scene):
        '''
        remove visible footsteps from one walker XXX
        '''

        #windex = walkers.index(walker)
        names = [
            "_S{}_{}_".format(
                self.object.name,
                self.feet.index(foot)) for foot in self.feet]
        obs = [
            ob for ob in scene.objects if
            ob.name.rstrip("0123456789") in names]
        for ob in obs:
            scene.objects.unlink(ob)

    def draw_footsteps(self, scene):
        '''
        draw the footsteps as empty objects on screen for editing
        '''
        self.remove_footsteps(scene)
        feet = self.feet
        rig = self.data
        indie_feet = [foot for foot in feet if foot.deps == []]
        total = len(indie_feet)
        draw_feet = feet if DEBUG else indie_feet
        for foot_no, foot in enumerate(draw_feet):
            footmat_name = '_atmn_{}_{}'.format(self.object.name, foot_no)
            try:
                footmat = bpy.data.materials[footmat_name]
            except:
                footmat = bpy.data.materials.new(name=footmat_name)
            scale = .1 + 1.8 * ((foot_no + .5) // 2) / total
            g = .3 * (scale - .1)
            if foot_no % 2:
                r = scale
                b = 0
            else:
                b = scale
                r = 0
            footmat.diffuse_color = Color((r, g, b))
            footsteps = foot.steps
            name = foot.name
            bone = rig.bones[name]
            try:
                stride = self.object.pose.bones[self.properties]['stride']
                foot_size = stride / 12
            except:
                foot_size = (bone.head - bone.tail).length * 2
            for index, footstep in enumerate(footsteps):
                stepname = "_S{}_{}_{}".format(
                    self.object.name,
                    feet.index(foot), footstep['frame'])
                groupname = "_SG{}_{}".format(
                    self.object.name,
                    footstep['frame'])
                props = {'frame': footstep['frame'], 'foot': name}
                make_marker(
                    bpy.context, stepname,
                    footstep['location'], footstep['rotation'], props,
                    foot_size * 1.0, footmat, groupname) # 1.0 is scale


