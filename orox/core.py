# Core - Generic UI applicable to all walkers, walker registration code,
# walker registry that keeps track of all walkers in action.
#
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

if "bpy" in locals():
    import importlib
    importlib.reload(parambulator)
else:
    from . import parambulator

import bpy
from bpy.app.handlers import persistent
from .parambulator import Parambulator

# ######################## Central Walker Services ############################


class Registry(object):
    """ Registry of Motor Vehicles (aka walkers) """

    def __init__(self):
        self.walkers = {} # Only lasts one session... pickle?
        self.enum_items = {("NONE", "None", "None", 0)} # for registration
        self.walker_types = []

    def append(self, walker):
        self.walkers[walker.object.name] = walker

    def __getitem__(self, ob):
        name = ob if type(ob) == str else ob.name
        if name in self.walkers:
            return self.walkers[name]
        else:
            return None

    def __iter__(self):
        self.list = list(self.walkers.keys())
        self.index = 0
        return self

    def __next__(self):
        try:
            key = self.list[self.index]
        except IndexError:
            raise StopIteration
        self.index += 1
        return self.walkers[key]

    def clear(self):
        """ On file load we should clear previous, non existant data """
        self.walkers.clear()

    def register(self, Walker):
        subtype = Walker.subtype
        self.walker_types.append(Walker)
        idx = len(self.walker_types)
        if subtype:
            self.enum_items.add(tuple([subtype] * 3 + [idx])) # Add a subtype to enum
        bpy.types.Object.orox_subtype = bpy.props.EnumProperty(
            name='Orox Subtype',
            items=list(self.enum_items))

    def reload_scene(self, context):
        """ Re find all the walkers in a scene """
        for walker_type in self.walker_types:
            walker_type.refresh(context, context.scene.objects)

    def register_prop(self):
        bpy.types.Object.orox_subtype = bpy.props.EnumProperty(
            name='Orox Subtype',
            items=list(self.enum_items))

    def unregister_prop(self):
        del(bpy.types.Object.orox_subtype)

registry = Registry()


def walkers():
    global registry
    return registry


@persistent
def file_load(dummy):
    """ Clear Registry before Load to avoid bad data """
    global registry
    registry.clear()
    registry.reload_scene(bpy.context)

# ############################## Core / Main UI ###############################


class RefreshScene(bpy.types.Operator):
    """ Refind Walkers in a saved scene """
    bl_idname = "object.find_walkers"
    bl_label = "Find Walkers"

    def execute(self, context):
        global registry
        registry.reload_scene(context)
        return {'FINISHED'}


class FootStepEdit(bpy.types.Operator):
    '''
    editable the Footsteps
    '''
    bl_idname = "object.edit_steps"
    bl_label = "Edit Footsteps"

    mode = bpy.props.EnumProperty(items=[
        ("EDIT", "Edit", "Draw Editable Footsteps"),
        ("INJEST", "Injest", "Injest Edited Footsteps"),
        ("CANCEL", "Cancel", "Delete Edited Footsteps"),
        ("HIDE", "Hide", "Hide Editable Footsteps")])

    @classmethod
    def poll(cls, context):
        return context.object.orox_subtype != 'NONE'

    def execute(self, context):
        global registry
        scene = context.scene
        ob = context.object
        mode = self.properties.mode
        walker = registry[ob]
        if walker:
            frames = range(
                scene.frame_preview_start, scene.frame_preview_end)
            if mode == "EDIT":
                # walker.set_action(bpy.data.actions["ref_normal"])
                # walker.bake_walk()
                walker.draw_footsteps(scene)
            elif mode == "INJEST":
                walker.injest_footsteps(scene)
                walker.bake_walk()
                walker.remove_footsteps(scene)
            else:
                walker.remove_footsteps(scene)
        return {'FINISHED'}


class FootStepPanel(bpy.types.Panel):
    """ Panel for generically editing and injesting Footsteps """

    bl_label = "Edit Footsteps"
    bl_idname = "SCENE_PT_orox_footsteps"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_category = "OROX"

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.operator("object.edit_steps", text="draw steps").mode = "EDIT"
        row = layout.row()
        row.operator("object.edit_steps", text="get steps").mode = "INJEST"
        row = layout.row()
        row.operator("object.edit_steps", text="delete steps").mode = "CANCEL"
        # row = layout.row()
        # row.operator("object.find_walkers")


class ParameterPanel(bpy.types.Panel):
    """ Parameters of Active Walker """

    bl_label = "Edit Parameters"
    bl_idname = "SCENE_PT_orox_parameters"
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_category = "OROX"

    def draw(self, context):
        layout = self.layout
        ob = context.object
        walker = registry[ob]
        if walker:
            row = layout.row()
            row.template_list(
                "ParamSelectionUl", "",
                ob, "autowalker_params", ob, "active_autowalk_param")


def update_prop(self, context):
    """ Update Parameter for a given walker """

    pass


class AutoWalkerParams(bpy.types.PropertyGroup):
    """  Parameters for the autowalkers """

    value = bpy.props.FloatProperty(
        default=0.0, min=-1.0, max=1.0, update=update_prop)


class ParamSelectionUl(bpy.types.UIList):
    """ UI for autowalker params """

    def draw_item(
            self, context, layout, data, item, icon,
            active_data, active_propname, index):
        param = item
        layout.label(
            text=param.name,
            translate=False, icon_value=icon)
        layout.prop(param, 'value', slider=True, text="")


def register():

    registry.register_prop()

    bpy.utils.register_class(AutoWalkerParams)
    bpy.utils.register_class(ParamSelectionUl)

    bpy.types.Object.autowalker_params = bpy.props.CollectionProperty(
        type=AutoWalkerParams)
    bpy.types.Object.active_autowalk_param = bpy.props.IntProperty()

    bpy.utils.register_class(FootStepEdit)
    bpy.utils.register_class(RefreshScene)
    bpy.utils.register_class(FootStepPanel)
    bpy.utils.register_class(ParameterPanel)

    bpy.app.handlers.load_post.append(file_load)

def unregister():

    bpy.app.handlers.load_post.remove(file_load)

    bpy.utils.unregister_class(FootStepPanel)
    bpy.utils.unregister_class(FootStepEdit)
    bpy.utils.unregister_class(RefreshScene)
    bpy.utils.unregister_class(ParameterPanel)

    registry.unregister_prop()

    del(bpy.types.Object.autowalker_params)
    del(bpy.types.Object.active_autowalk_param)

    bpy.utils.unregister_class(ParamSelectionUl)
    bpy.utils.unregister_class(AutoWalkerParams)
